package ru.alia.urfin;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;


class UrfinTest {
    private static WebDriver driver;

    @BeforeAll
    static void init() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        options.addArguments("enable-automation");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--disable-gpu");
        driver = new ChromeDriver(options);
    }

    @AfterAll
    static void clear() {
        driver.close();
    }

    @Test
    void urfinTest() {
        // Перейти в браузере на yandex.ru
        driver.get("https://www.yandex.ru/");

        String oldWindow = driver.getWindowHandle();

        // Вбить в поиске Яндекс почта
        WebElement textElement = driver.findElement(By.xpath("//input[@id='text']"));
        textElement.sendKeys("яндекс почта");

        // Нажать кнопку найти
        WebElement buttonElement = driver.findElement(By.xpath("//button[@type='submit']"));
        buttonElement.click();

        // В результатах поиска выбрать и перейти на страницу Яндекс Почты
        WebElement linkElement = driver.findElement(By.xpath("//a[@href='https://mail.yandex.ru/']/div[@class='organic__url-text']"));
        linkElement.click();

        // В результатах поиска выбрать и перейти на страницу Яндекс Почты
        for (String winHandle : driver.getWindowHandles()) {
            if (!oldWindow.equals(winHandle)) {
                driver.switchTo().window(winHandle);
            }
        }

        // Проскроллить страницу вниз
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,10000)");

        // Нажать кнопку Войти
        WebElement loginButtonElement = driver.findElement(By.xpath("//a[contains(@class,'HeadBanner-Button-Enter')]"));
        loginButtonElement.click();

        // В поле Логин ввести случайное значение (Каждый прогон теста уникальное)
        WebElement loginElement = driver.findElement(By.xpath("//input[@id='passp-field-login']"));
        loginElement.sendKeys(UUID.randomUUID().toString());

        // Нажать кнопку Войти
        WebElement enterButtonElement = driver.findElement(By.xpath("//button[@type='submit']"));
        enterButtonElement.click();

        // Проверить наличие ошибки: Такой логин не подойдет
        WebElement targetElement = driver.findElement(By.xpath("//div[@class='passp-form-field__input']/following-sibling::div"));
        assertTrue(Set.of(
                "Такой логин не подойдет",
                "Не помню логин",
                "Такого аккаунта нет"
        ).contains(targetElement.getText()));
    }
}

